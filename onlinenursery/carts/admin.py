from django.contrib import admin

from .models import Cart

class CartAdmin(admin.ModelAdmin):
    list_display = ['user','subtotal','total','timestamp']
    list_filter = ('user','plants','total')

    # inlines = [ProductFileInline]
    
    class Meta:
        model = Cart
admin.site.register(Cart,CartAdmin)
