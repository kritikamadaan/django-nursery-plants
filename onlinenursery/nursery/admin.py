from django.contrib import admin

# Register your models here.
from .models import Plant

# class ProductFileInline(admin.TabularInline):
#     model = ProductFile
#     extra = 1

class PlantAdmin(admin.ModelAdmin):
    list_display = ['name','price','inventory','featured']
    list_filter = ('user','name','price','featured')

    # inlines = [ProductFileInline]
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('name', 'price', 'featured')}
        ),
    )
    class Meta:
        model = Plant

admin.site.register(Plant, PlantAdmin)
